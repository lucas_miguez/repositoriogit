
if (select count(1) from sysobjects o where o.name = 'TipoTelefone' and o.type ='U' ) = 0
begin
	CREATE TABLE [dbo].[TipoTelefone](
		[Id] [uniqueidentifier] NOT NULL,
		[TipoTelefone_Descricao] [varchar](50) NOT NULL,
		[TipoTelefone_Ativo] [bit] NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 65) ON [PRIMARY]
	) ON [PRIMARY]

	--insert into TipoTelefone values ('4F96C687-1FA1-48B0-AD74-41CFB340D7CB','Comercial',1)
	--insert into TipoTelefone values ('5E38D04C-E7AC-48F4-8BA3-FB2CF3FB0160','Residencial',1)
	--insert into TipoTelefone values ('84784C39-45E5-41D0-9B5B-722781ABE924','Celular',1)
end

if (select count(1) from sysobjects o where o.name = 'EstadoCivil' and o.type ='U' ) = 0
begin
	CREATE TABLE [dbo].[EstadoCivil](
		[EstadoCivil_Codigo] [uniqueidentifier] NOT NULL,
		[EstadoCivil_Descricao] [varchar](50) NOT NULL,
		[EstadoCivil_Ativo] [bit] NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[EstadoCivil_Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 65) ON [PRIMARY]
	) ON [PRIMARY]

	--DECLARE @myid uniqueidentifier  
	--SET @myid = NEWID()  
	--PRINT 'Value of @myid is: '+ CONVERT(varchar(255), @myid) 
	--insert into EstadoCivil values ('4F96C687-1FA1-48B0-AD74-41CFB340D7CB','Casado',1)
	--insert into EstadoCivil values ('5E38D04C-E7AC-48F4-8BA3-FB2CF3FB0160','Solteiro',1)
	--insert into EstadoCivil values ('84784C39-45E5-41D0-9B5B-722781ABE924','Divorciado',1)
end


if (Select count(1) from sysobjects o where o.name = 'Profissao' and o.type = 'U') = 0 
begin 	
	CREATE TABLE [dbo].[Profissao](
		[Id] [uniqueidentifier] NOT NULL,
		[Descricao] [varchar](200) NOT NULL,
		[Ativo] [bit] NOT NULL,
	 CONSTRAINT [PK_Profissao] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
end

go


if (Select count(1) from sysobjects o where o.name = 'Escolaridade' and o.type = 'U') = 0 
begin 	
	CREATE TABLE [dbo].[Escolaridade](
		[Id] [uniqueidentifier] NOT NULL,
		[Descricao] [varchar](200) NOT NULL,
		[Ativo] [bit] NOT NULL,
	 CONSTRAINT [PK_Escolaridade] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
end

go



if (Select count(1) from sysobjects o where o.name = 'Pessoa' and o.type = 'U') = 0 
begin 	
	CREATE TABLE [dbo].[Pessoa](
		[Id] [uniqueidentifier] NOT NULL,
		[Nome] [varchar](60) NOT NULL,
		[NomeFantasia] [varchar](60) NULL,
		[TipoPessoa] [char](1) NOT NULL,
		[CpfCnpj] [varchar](20) NOT NULL,
		[RG] [varchar](40) NOT NULL,
		[Pessoa_DataNascFund] [datetime] NOT NULL,
		[Pessoa_Sexo] [char](1) NULL,
		[EstadoCivil_Codigo] [uniqueidentifier] NULL,
		[Escolaridade_Codigo] [uniqueidentifier] NULL,
		[Profissao_Codigo] [uniqueidentifier] NULL,
		[Pessoa_Email] [varchar](150) NOT NULL,
		[Pessoa_Ativo] [bit] NOT NULL
	 CONSTRAINT [PK_Pessoa] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[Pessoa]  WITH CHECK ADD  CONSTRAINT [FK_Pessoa_Escolaridade] FOREIGN KEY([Escolaridade_Codigo])
	REFERENCES [dbo].[Escolaridade] (ID)
	
	ALTER TABLE [dbo].[Pessoa] CHECK CONSTRAINT [FK_Pessoa_Escolaridade]
	
	ALTER TABLE [dbo].[Pessoa]  WITH CHECK ADD  CONSTRAINT [FK_Pessoa_EstadoCivil] FOREIGN KEY([EstadoCivil_Codigo])
	REFERENCES [dbo].[EstadoCivil] (Id)
	ALTER TABLE [dbo].[Pessoa] CHECK CONSTRAINT [FK_Pessoa_EstadoCivil]

	ALTER TABLE [dbo].[Pessoa]  WITH CHECK ADD  CONSTRAINT [FK_Pessoa_Profissao] FOREIGN KEY([Profissao_Codigo])
	REFERENCES [dbo].[Profissao] (Id)
	ALTER TABLE [dbo].[Pessoa] CHECK CONSTRAINT [FK_Pessoa_Profissao]
end 


if (Select count(1) from sysobjects o where o.name = 'PessoaTelefone' and o.type = 'U') = 0 
begin 
	CREATE TABLE [dbo].[PessoaTelefone](
		[Pessoa_Codigo] [smallint] NOT NULL,
		[PessoaTelefone_Codigo] [smallint] NOT NULL,
		[Pessoa_TipoTelefone_Codigo] [smallint] NOT NULL,
		[PessoaTeleFone_DDD] [char](6) NOT NULL,
		[PessoaTelefone_Fone] [varchar](14) NOT NULL,
		[PessoaTelefone_Ativo] [bit] NOT NULL
	CONSTRAINT [PK_PessoaTelefone] PRIMARY KEY CLUSTERED 
	(
		[Pessoa_Codigo] ASC,
		[PessoaTelefone_Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	--	ALTER TABLE [dbo].[Pessoa]  WITH CHECK ADD  CONSTRAINT [FK_Pessoa_Profissao] FOREIGN KEY([Profissao_Codigo])
	--REFERENCES [dbo].[Profissao] ([Profissao_Codigo])
	--ALTER TABLE [dbo].[Pessoa] CHECK CONSTRAINT [FK_Pessoa_Profissao]



	ALTER TABLE [dbo].[PessoaTelefone]  WITH CHECK ADD  CONSTRAINT [FK_PessoaTelefone_TipoTelefone] FOREIGN KEY([Pessoa_TipoTelefone_Codigo])
	REFERENCES [dbo].[TipoTelefone] (Id)
	ALTER TABLE [dbo].[PessoaTelefone] CHECK CONSTRAINT [FK_Pessoa_TipoTelefone_Codigo]
END


--if (Select count(1) from sysobjects o where o.name = 'EstadoCivil' and o.type = 'U') = 0 
--begin 	
--	CREATE TABLE [dbo].[EstadoCivil](
--		[EstadoCivil_Codigo] [smallint] IDENTITY(1,1) NOT NULL,
--		[EstadoCivil_Descricao] [varchar](200) NOT NULL,
--		[EstadoCivil_Ativo] [bit] NOT NULL
--	PRIMARY KEY CLUSTERED 
--	(
--		[EstadoCivil_Codigo] ASC
--	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 65) ON [PRIMARY]
--	) ON [PRIMARY]
--end 

--ALTER TABLE [dbo].[Pessoa] DROP CONSTRAINT [FK_Pessoa_FaixaRenda]
--GO

--ALTER TABLE [dbo].[Pessoa] DROP CONSTRAINT [FK_Pessoa_EstadoCivil]
--GO

--ALTER TABLE [dbo].[Pessoa] DROP CONSTRAINT [FK_Pessoa_Escolaridade]
--GO

--/****** Object:  Table [dbo].[Pessoa]    Script Date: 08/06/2020 20:47:00 ******/
--DROP TABLE IF EXISTS [dbo].[Pessoa]
--GO

--/****** Object:  Table [dbo].[Pessoa]    Script Date: 08/06/2020 20:47:00 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[Pessoa](
--	[Pessoa_Codigo] [smallint] IDENTITY(1,1) NOT NULL,
--	[Pessoa_Nome] [varchar](60) NOT NULL,
--	[Pessoa_TipoPessoa] [char](1) NOT NULL,
--	[Pessoa_Doc] [varchar](20) NOT NULL,
--	[Pessoa_DataNascimento] [datetime] NOT NULL,
--	[Pessoa_Sexo] [char](1) NULL,
--	[EstadoCivil_Codigo] [smallint] NULL,
--	[Escolaridade_Codigo] [smallint] NULL,
--	[Profissao_Codigo] [smallint] NULL,
--	[FaixaRenda_Codigo] [smallint] NULL,
--	[Pessoa_Email] [varchar](150) NOT NULL,
--	[Pessoa_NomeFantasia] [varchar](60) NULL,
--	[Pessoa_AceitaSMS] [bit] NOT NULL,
--	[Pessoa_AceitaEmail] [bit] NOT NULL,
--	[Pessoa_Ativo] [bit] NOT NULL,
--	[Pessoa_Fone1] [varchar](14) NULL,
-- CONSTRAINT [PK_Pessoa] PRIMARY KEY CLUSTERED 
--(
--	[Pessoa_Codigo] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--ALTER TABLE [dbo].[Pessoa]  WITH CHECK ADD  CONSTRAINT [FK_Pessoa_Escolaridade] FOREIGN KEY([Escolaridade_Codigo])
--REFERENCES [dbo].[Escolaridade] ([Escolaridade_Codigo])
--GO

--ALTER TABLE [dbo].[Pessoa] CHECK CONSTRAINT [FK_Pessoa_Escolaridade]
--GO

--ALTER TABLE [dbo].[Pessoa]  WITH CHECK ADD  CONSTRAINT [FK_Pessoa_EstadoCivil] FOREIGN KEY([EstadoCivil_Codigo])
--REFERENCES [dbo].[EstadoCivil] ([EstadoCivil_Codigo])
--GO

--ALTER TABLE [dbo].[Pessoa] CHECK CONSTRAINT [FK_Pessoa_EstadoCivil]
--GO

--ALTER TABLE [dbo].[Pessoa]  WITH CHECK ADD  CONSTRAINT [FK_Pessoa_FaixaRenda] FOREIGN KEY([FaixaRenda_Codigo])
--REFERENCES [dbo].[FaixaRenda] ([FaixaRenda_Codigo])
--GO

--ALTER TABLE [dbo].[Pessoa] CHECK CONSTRAINT [FK_Pessoa_FaixaRenda]
--GO


--DROP TABLE IF EXISTS [dbo].[EstadoCivil]
--GO

--/****** Object:  Table [dbo].[Enderecos]    Script Date: 08/06/2020 20:29:10 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[EstadoCivil](
--	[EstadoCivil_Codigo] [smallint] IDENTITY(1,1) NOT NULL,
--	[EstadoCivil_Descricao] [varchar](50) NOT NULL,
--	[EstadoCivil_Ativo] [bit] NOT NULL,
--	[EstadoCivil_UsuCodCriacao] [int] NOT NULL,
--	[EstadoCivil_DataCriacao] [datetime] NOT NULL,
--	[EstadoCivil_UsuCodAlteracao] [int] NULL,
--	[EstadoCivil_DataAlteracao] [datetime] NULL,
--PRIMARY KEY CLUSTERED 
--(
--	[EstadoCivil_Codigo] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 65) ON [PRIMARY]
--) ON [PRIMARY]
--GO	
--DROP TABLE IF EXISTS [dbo].[Escolaridade]


--GO

--/****** Object:  Table [dbo].[Enderecos]    Script Date: 08/06/2020 20:29:10 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[Escolaridade](
--	[Escolaridade_Codigo] [smallint] IDENTITY(1,1) NOT NULL,
--	[Escolaridade_Descricao] [varchar](50) NOT NULL,
--	[Escolaridade_Ativo] [bit] NOT NULL,
--	[Escolaridade_UsuCodCriacao] [int] NOT NULL,
--	[Escolaridade_DataCriacao] [datetime] NOT NULL,
--	[Escolaridade_UsuCodAlteracao] [int] NULL,
--	[Escolaridade_DataAlteracao] [datetime] NULL,
--PRIMARY KEY CLUSTERED 
--(
--	[Escolaridade_Codigo] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 65) ON [PRIMARY]
--) ON [PRIMARY]
--GO
--DROP TABLE IF EXISTS [dbo].[TipoEndereco]

--GO
--CREATE TABLE [dbo].[TipoEndereco](
--	[TipoEndereco_Codigo] [smallint] IDENTITY(1,1) NOT NULL,
--	[TipoEndereco_Descricao] [varchar](50) NOT NULL,
--	[TipoEndereco_Ativo] [bit] NOT NULL,
--	[TipoEndereco_UsuCodCriacao] [int] NOT NULL,
--	[TipoEndereco_DataCriacao] [datetime] NOT NULL,
--	[TipoEndereco_UsuCodAlteracao] [int] NULL,
--	[TipoEndereco_DataAlteracao] [datetime] NULL,
--PRIMARY KEY CLUSTERED 
--(
--	[TipoEndereco_Codigo] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 65) ON [PRIMARY]
--) ON [PRIMARY]
--GO




