﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace DevIO.Data.Repository
{

    public class FaixaRendaRepository : Repository<FaixaRenda>, IFaixaRendaRepository
    {
        public FaixaRendaRepository(MeuDbContext context) : base(context) { }
        public Task<FaixaRenda> ObterFornecedorEndereco(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<FaixaRenda> ObterFornecedorProdutosEndereco(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
