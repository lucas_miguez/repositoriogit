﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace DevIO.Data.Repository
{
    public class EstadoCivilRepository : Repository<EstadoCivil>, IEstadoCivilRepository
    {
        public EstadoCivilRepository(MeuDbContext context) : base(context) { }

        public async Task<EstadoCivil> ObterEstadoCivil(Guid id)
        {
            return await Db.EstadoCivil.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);


        }
    }
}
