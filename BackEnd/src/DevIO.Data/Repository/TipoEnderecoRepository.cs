﻿using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Data.Repository
{
    public class TipoEnderecoRepository : Repository<TipoEndereco>, ITipoEnderecoRepository
    {
        public TipoEnderecoRepository(MeuDbContext context) : base(context) { }

        public Task<IEnumerable<TipoEndereco>> ObterTipoEndereco(Guid TipoEnderecoId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<TipoEndereco>> ObterTipoEndereco()
        {
            throw new NotImplementedException();
        }

        public Task<TipoEndereco> ObterTipoEnderecoEnd(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
