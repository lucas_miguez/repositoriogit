using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace DevIO.Data.Repository
{
    public class EscolaridadeRepository : Repository<Escolaridade>, IEscolaridadeRepository
    {
        public EscolaridadeRepository(MeuDbContext context) : base(context) { }
        public async Task<Escolaridade> ObterEscolaridade(Guid id)
        {
            return await Db.Escolaridade.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);
        }

    }

}
