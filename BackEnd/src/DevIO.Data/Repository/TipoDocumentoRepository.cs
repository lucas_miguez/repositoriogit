﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace DevIO.Data.Repository
{
    public class TipoDocumentoRepository : Repository<TipoDocumento>, ITipoDocumentoRepository
    {
        public TipoDocumentoRepository(MeuDbContext context) : base(context) { }

        public async Task<TipoDocumento> ObterTipoDocumento(Guid id)
        {
            return await Db.TipoDocumento.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);
        }
        
    }
}