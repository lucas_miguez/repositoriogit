﻿using AutoMapper;
using DevIO.Api.ViewModels;
using DevIO.Business.Models;

namespace DevIO.Api.Configuration
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig()
        {
            CreateMap<Fornecedor, FornecedorViewModel>().ReverseMap();
            CreateMap<Endereco, EnderecoViewModel>().ReverseMap();

            CreateMap<ProdutoViewModel, Produto>();
            CreateMap<ProdutoImagemViewModel, Produto>().ReverseMap();

            CreateMap<TipoDocumentoViewModel, TipoDocumento>();
            CreateMap<TipoDocumentoViewModel, TipoDocumento>().ReverseMap();

            CreateMap<EscolaridadeViewModel, Escolaridade>();
            CreateMap<EscolaridadeViewModel, Escolaridade>().ReverseMap();

            CreateMap<ProfissaoViewModel, Profissao>();
            CreateMap<ProfissaoViewModel, Profissao>().ReverseMap();

            CreateMap<EstadoCivilViewModel, EstadoCivil>();
            CreateMap<EstadoCivilViewModel, EstadoCivil>().ReverseMap();


            CreateMap<Produto, ProdutoViewModel>()
                .ForMember(dest => dest.NomeFornecedor, opt => opt.MapFrom(src => src.Fornecedor.Nome));
        }
    }
}