﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DevIO.Api.ViewModels
{
    public class TipoEnderecoViewModel
    {
        [Key]
        public int TipoEndereco_Codigo { get; set; }
        public string TipoEndereco_Descricao { get; set; }


        public Boolean TipoEndereco_Ativo { get; set; }

        public int TipoEndereco_UsuCodCriacao { get; set; }

        public DateTime TipoEndereco_DataCriacao { get; set; }

        public int TipoEndereco_UsuCodAlteracao { get; set; }
        public DateTime TipoEndereco_DataAlteracao { get; set; }
    }
}
