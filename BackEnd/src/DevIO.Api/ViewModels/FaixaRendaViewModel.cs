﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DevIO.Api.ViewModels
{
    public class FaixaRendaViewModel
    {
        [Key]
        public int FaixaRenda_Codigo { get; set; }

        public string FaixaRenda_Descricao { get; set; }

        public Boolean FaixaRenda_Ativo { get; set; }


        public int FaixaRenda_UsuCodCriacao { get; set; }

        public DateTime FaixaRenda_DataCriacao { get; set; }

        public int FaixaRenda_UsuCodAlteracao { get; set; }
        public DateTime FaixaRenda_DataAlteracao { get; set; }
    }
}
