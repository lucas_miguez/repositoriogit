﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DevIO.Api.ViewModels
{
    public class PessoaViewModel
    {
		[Key]
		public int Pessoa_Codigo { get; set; }
		public string Pessoa_Nome { get; set; }
		public int Pessoa_TipoPessoa { get; set; }

		public int Pessoa_Doc { get; set; }
		public DateTime Pessoa_DataNascimento { get; set; }
		public int Pessoa_Sexo { get; set; }
		public int EstadoCivil_Codigo { get; set; }

		public int Escolaridade_Codigo { get; set; }

		public int Profissao_Codigo { get; set; }

		public int FaixaRenda_Codigo { get; set; }

		public string Pessoa_Email { get; set; }

		public string Pessoa_NomeFantasia { get; set; }

		public Boolean Pessoa_AceitaSMS { get; set; }

		public Boolean Pessoa_AceitaEmail { get; set; }

		public Boolean Pessoa_Ativo { get; set; }

		public int Pessoa_Telefone { get; set; }
	}
}
