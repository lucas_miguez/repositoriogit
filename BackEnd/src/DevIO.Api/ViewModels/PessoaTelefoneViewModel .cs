﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DevIO.Api.ViewModels
{
    public class PessoaTeleFoneViewModel
	{
		[Key]
		public int Pessoa_Codigo { get; set; }
		public string PessoaTeleFone_DDD { get; set; }
		public string PessoaTelefone_Fone { get; set; }

		public int PessoaTelefone_UsuCodCriacao { get; set; }
		public DateTime PessoaTelefone_DataCriacao { get; set; }
		public int PessoaTelefone_UsuCodAlteracao { get; set; }

		public int PessoaTelefone_Ativo { get; set; }
	}
}
