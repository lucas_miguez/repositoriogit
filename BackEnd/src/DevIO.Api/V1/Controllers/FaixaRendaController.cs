﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DevIO.Api.Controllers;
using DevIO.Api.ViewModels;
using DevIO.Business.Intefaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DevIO.Api.V1.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/FaixaRendas")]
    public class FaixaRendaController : MainController
    {
        private readonly IFaixaRendaRepository _FaixaRendaRepository;
        private readonly IFaixaRendaServices _IFaixaRendaServices;
        private readonly IEnderecoRepository _enderecoRepository;
        private readonly IMapper _mapper;

        public FaixaRendaController(IFaixaRendaRepository faixaRendaRepository,
                                      IMapper mapper,
                                      IFaixaRendaServices faixaRendaServices,
                                      INotificador notificador,
                                      IEnderecoRepository enderecoRepository,
                                      IUser user) : base(notificador,user)
        {
            _FaixaRendaRepository = faixaRendaRepository;
            _mapper = mapper;
            _IFaixaRendaServices = faixaRendaServices;
            _enderecoRepository = enderecoRepository;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<FaixaRendaViewModel>> ObterTodos()
        {
            return _mapper.Map<IEnumerable<FaixaRendaViewModel>>(await _FaixaRendaRepository.ObterTodos());
        }
    }
}