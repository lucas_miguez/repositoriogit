﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using DevIO.Api.Controllers;
using DevIO.Api.Extensions;
using DevIO.Api.ViewModels;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace DevIO.Api.V1.Controllers
{
    //[Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/EstadoCivil")]
    public class EstadoCivilController : MainController
    {

        private readonly IEstadoCivilRepository _EstadoCivilRepository;
        private readonly IEstadoCivilService _EstadoCivilService;
        private readonly IMapper _mapper;

        public EstadoCivilController(
                                   INotificador notificador,
                                   IEstadoCivilRepository EstadoCivilRepository,
                                   IEstadoCivilService EstadoCivilService,
                                   IMapper mapper,
                                   IUser user) : base(notificador, user)
        {
            _EstadoCivilRepository = EstadoCivilRepository;
            _EstadoCivilService = EstadoCivilService;
            _mapper = mapper;
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpGet]
        public async Task<ActionResult> ObterTodos()
        {
            var tiposdocs = _mapper.Map<IEnumerable<EstadoCivilViewModel>>(await _EstadoCivilRepository.ObterTodos());
            return CustomResponse(tiposdocs);
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EstadoCivilViewModel>> ObterPorId(Guid id)
        {
            var EstadoCivilViewModel = await ObterTipoDocumento(id);

            if (EstadoCivilViewModel == null) return NotFound();

            return CustomResponse(EstadoCivilViewModel);
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpPost]
        public async Task<ActionResult<EstadoCivilViewModel>> Adicionar(EstadoCivilViewModel EstadoCivilViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            if (EstadoCivilViewModel.Id.ToString() == "00000000-0000-0000-0000-000000000000")
            {
                EstadoCivilViewModel.Id = Guid.NewGuid();
            }

            await _EstadoCivilService.Adicionar(_mapper.Map<EstadoCivil>(EstadoCivilViewModel));

            return CustomResponse(EstadoCivilViewModel);
        }


        [ClaimsAuthorize("Admin", "1")]
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Atualizar(Guid id, EstadoCivilViewModel EstadoCivilViewModel)
        {
            try
            {
                if (id != EstadoCivilViewModel.Id)
                {
                    NotificarErro("Os ids informados não são iguais!");
                    return CustomResponse();
                }

                if (EstadoCivilViewModel.Id.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    NotificarErro("ID não informado!");
                    return CustomResponse();
                }


                if (!ModelState.IsValid) return CustomResponse(ModelState);


                await _EstadoCivilService.Atualizar(_mapper.Map<EstadoCivil>(EstadoCivilViewModel));

                return CustomResponse(EstadoCivilViewModel);
            }
            catch (Exception ex)
            {
                NotificarErro(ex.Message.ToString());
                return CustomResponse();
            }
        }

        private async Task<EstadoCivilViewModel> ObterTipoDocumento(Guid id)
        {
            return _mapper.Map<EstadoCivilViewModel>(await _EstadoCivilRepository.ObterPorId(id));
        }



    }
}