﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using DevIO.Api.Controllers;
using DevIO.Api.Extensions;
using DevIO.Api.ViewModels;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace DevIO.Api.V1.Controllers
{


    //[Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/Profissao")]
    public class ProfissaoController : MainController
    {

        private readonly IProfissaoRepository _ProfissaoRepository;
        private readonly IProfissaoService _ProfissaoService;
        private readonly IMapper _mapper;

        public ProfissaoController(
                                   INotificador notificador,
                                   IProfissaoRepository ProfissaoRepository,
                                   IProfissaoService ProfissaoService,
                                   IMapper mapper,
                                   IUser user) : base(notificador, user)
        {
            _ProfissaoRepository = ProfissaoRepository;
            _ProfissaoService = ProfissaoService;
            _mapper = mapper;
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpGet]
        public async Task<ActionResult> ObterTodos()
        {
            var tiposdocs = _mapper.Map<IEnumerable<ProfissaoViewModel>>(await _ProfissaoRepository.ObterTodos());
            return CustomResponse(tiposdocs);
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<ProfissaoViewModel>> ObterPorId(Guid id)
        {
            var ProfissaoViewModel = await ObterTipoDocumento(id);

            if (ProfissaoViewModel == null) return NotFound();

            return CustomResponse(ProfissaoViewModel);
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpPost]
        public async Task<ActionResult<ProfissaoViewModel>> Adicionar(ProfissaoViewModel ProfissaoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            if (ProfissaoViewModel.Id.ToString() == "00000000-0000-0000-0000-000000000000")
            {
                ProfissaoViewModel.Id = Guid.NewGuid();
            }

            await _ProfissaoService.Adicionar(_mapper.Map<Profissao>(ProfissaoViewModel));

            return CustomResponse(ProfissaoViewModel);
        }


        [ClaimsAuthorize("Admin", "1")]
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Atualizar(Guid id, ProfissaoViewModel ProfissaoViewModel)
        {
            try
            {
                if (id != ProfissaoViewModel.Id)
                {
                    NotificarErro("Os ids informados não são iguais!");
                    return CustomResponse();
                }

                if (ProfissaoViewModel.Id.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    NotificarErro("ID não informado!");
                    return CustomResponse();
                }


                if (!ModelState.IsValid) return CustomResponse(ModelState);


                await _ProfissaoService.Atualizar(_mapper.Map<Profissao>(ProfissaoViewModel));

                return CustomResponse(ProfissaoViewModel);
            }
            catch (Exception ex)
            {
                NotificarErro(ex.Message.ToString());
                return CustomResponse();
            }
        }

        private async Task<ProfissaoViewModel> ObterTipoDocumento(Guid id)
        {
            return _mapper.Map<ProfissaoViewModel>(await _ProfissaoRepository.ObterPorId(id));
        }



    }
}