﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DevIO.Business.Models;
using DevIO.Data.Context;

namespace DevIO.Api.V1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoEnderecoesController : ControllerBase
    {
        private readonly MeuDbContext _context;

        public TipoEnderecoesController(MeuDbContext context)
        {
            _context = context;
        }

        // GET: api/TipoEnderecoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoEndereco>>> GetTipoEnderecos()
        {
            return await _context.TipoEnderecos.ToListAsync();
        }

        // GET: api/TipoEnderecoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoEndereco>> GetTipoEndereco(Guid id)
        {
            var tipoEndereco = await _context.TipoEnderecos.FindAsync(id);

            if (tipoEndereco == null)
            {
                return NotFound();
            }

            return tipoEndereco;
        }

        // PUT: api/TipoEnderecoes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoEndereco(Guid id, TipoEndereco tipoEndereco)
        {
            if (id != tipoEndereco.Id)
            {
                return BadRequest();
            }

            _context.Entry(tipoEndereco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoEnderecoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TipoEnderecoes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TipoEndereco>> PostTipoEndereco(TipoEndereco tipoEndereco)
        {
            _context.TipoEnderecos.Add(tipoEndereco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTipoEndereco", new { id = tipoEndereco.Id }, tipoEndereco);
        }

        // DELETE: api/TipoEnderecoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TipoEndereco>> DeleteTipoEndereco(Guid id)
        {
            var tipoEndereco = await _context.TipoEnderecos.FindAsync(id);
            if (tipoEndereco == null)
            {
                return NotFound();
            }

            _context.TipoEnderecos.Remove(tipoEndereco);
            await _context.SaveChangesAsync();

            return tipoEndereco;
        }

        private bool TipoEnderecoExists(Guid id)
        {
            return _context.TipoEnderecos.Any(e => e.Id == id);
        }
    }
}
