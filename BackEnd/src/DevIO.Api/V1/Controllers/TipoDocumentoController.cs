﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using DevIO.Api.Controllers;
using DevIO.Api.Extensions;
using DevIO.Api.ViewModels;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DevIO.Api.V1.Controllers
{

    //[Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/tipodocumento")]
    public class TipoDocumentoController : MainController
    {
        private readonly ITipoDocumentoRepository _tipoDocumentoRepository;
        private readonly ITipoDocumentoService _tipoDocumentoService;
        private readonly IMapper _mapper;

        public TipoDocumentoController(
                                  INotificador notificador,
                                  ITipoDocumentoRepository tipoDocumentoRepository,
                                  ITipoDocumentoService tipoDocumentoService,
                                  IMapper mapper,
                                  IUser user) : base(notificador, user)
        {
            _tipoDocumentoRepository = tipoDocumentoRepository;
            _tipoDocumentoService = tipoDocumentoService;
            _mapper = mapper;
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpGet]
        public async Task<ActionResult> ObterTodos()
        {
              var tiposdocs =_mapper.Map<IEnumerable<TipoDocumentoViewModel>>(await _tipoDocumentoRepository.ObterTodos());
            return CustomResponse(tiposdocs);
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<TipoDocumentoViewModel>> ObterPorId(Guid id)
        {
            var tipoDocumentoViewModel = await ObterTipoDocumento(id);

            if (tipoDocumentoViewModel == null) return NotFound();

            return CustomResponse(tipoDocumentoViewModel);
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpPost]
        public async Task<ActionResult<TipoDocumentoViewModel>> Adicionar(TipoDocumentoViewModel tipoDocumentoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            if (tipoDocumentoViewModel.Id.ToString() == "00000000-0000-0000-0000-000000000000")
            {
                tipoDocumentoViewModel.Id = Guid.NewGuid();
            }

            
            await _tipoDocumentoService.Adicionar(_mapper.Map<TipoDocumento>(tipoDocumentoViewModel));

            return CustomResponse(tipoDocumentoViewModel);
        }


        [ClaimsAuthorize("Admin", "1")]
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Atualizar(Guid id, TipoDocumentoViewModel tipoDocumentoViewModel)
        {
            try 
            { 
                if (id != tipoDocumentoViewModel.Id)
                {
                    NotificarErro("Os ids informados não são iguais!");
                    return CustomResponse();
                }

                if (tipoDocumentoViewModel.Id.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    NotificarErro("ID não informado!");
                    return CustomResponse();
                }


                //var tipoDocumentoAtualizcao = await ObterTipoDocumento(id);

                if (!ModelState.IsValid) return CustomResponse(ModelState);

                //tipoDocumentoAtualizcao.Descricao = tipoDocumentoViewModel.Descricao;
                //tipoDocumentoAtualizcao.Ativo = tipoDocumentoViewModel.Ativo;

                await _tipoDocumentoService.Atualizar(_mapper.Map<TipoDocumento>(tipoDocumentoViewModel));

                return CustomResponse(tipoDocumentoViewModel);
            }
            catch (Exception ex)
            {
                 NotificarErro(ex.Message.ToString());
                 return CustomResponse();
            }
        }

        private async Task<TipoDocumentoViewModel> ObterTipoDocumento(Guid id)
        {
            return _mapper.Map<TipoDocumentoViewModel>(await _tipoDocumentoRepository.ObterPorId(id));
            
            
        }



    }
}
