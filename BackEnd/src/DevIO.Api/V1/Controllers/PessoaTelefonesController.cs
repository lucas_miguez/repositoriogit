﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DevIO.Business.Models;
using DevIO.Data.Context;

namespace DevIO.Api.V1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PessoaTelefonesController : ControllerBase
    {
        private readonly MeuDbContext _context;

        public PessoaTelefonesController(MeuDbContext context)
        {
            _context = context;
        }

        // GET: api/PessoaTelefones
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PessoaTelefone>>> GetpessoaTelefones()
        {
            return await _context.pessoaTelefones.ToListAsync();
        }

        // GET: api/PessoaTelefones/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PessoaTelefone>> GetPessoaTelefone(Guid id)
        {
            var pessoaTelefone = await _context.pessoaTelefones.FindAsync(id);

            if (pessoaTelefone == null)
            {
                return NotFound();
            }

            return pessoaTelefone;
        }

        // PUT: api/PessoaTelefones/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPessoaTelefone(Guid id, PessoaTelefone pessoaTelefone)
        {
            if (id != pessoaTelefone.Id)
            {
                return BadRequest();
            }

            _context.Entry(pessoaTelefone).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PessoaTelefoneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PessoaTelefones
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<PessoaTelefone>> PostPessoaTelefone(PessoaTelefone pessoaTelefone)
        {
            _context.pessoaTelefones.Add(pessoaTelefone);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPessoaTelefone", new { id = pessoaTelefone.Id }, pessoaTelefone);
        }

        // DELETE: api/PessoaTelefones/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PessoaTelefone>> DeletePessoaTelefone(Guid id)
        {
            var pessoaTelefone = await _context.pessoaTelefones.FindAsync(id);
            if (pessoaTelefone == null)
            {
                return NotFound();
            }

            _context.pessoaTelefones.Remove(pessoaTelefone);
            await _context.SaveChangesAsync();

            return pessoaTelefone;
        }

        private bool PessoaTelefoneExists(Guid id)
        {
            return _context.pessoaTelefones.Any(e => e.Id == id);
        }
    }
}
