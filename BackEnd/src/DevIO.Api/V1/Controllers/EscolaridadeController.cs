﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using DevIO.Api.Controllers;
using DevIO.Api.Extensions;
using DevIO.Api.ViewModels;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace DevIO.Api.V1.Controllers
{


    //[Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/escolaridade")]
    public class EscolaridadeController : MainController
    {

        private readonly IEscolaridadeRepository _escolaridadeRepository;
        private readonly IEscolaridadeService _escolaridadeService;
        private readonly IMapper _mapper;

        public EscolaridadeController(
                                   INotificador notificador,
                                   IEscolaridadeRepository escolaridadeRepository,
                                   IEscolaridadeService escolaridadeService,
                                   IMapper mapper,
                                   IUser user) : base(notificador, user)
        {
            _escolaridadeRepository = escolaridadeRepository;
            _escolaridadeService = escolaridadeService;
            _mapper = mapper;
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpGet]
        public async Task<ActionResult> ObterTodos()
        {
            var tiposdocs = _mapper.Map<IEnumerable<EscolaridadeViewModel>>(await _escolaridadeRepository.ObterTodos());
            return CustomResponse(tiposdocs);
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EscolaridadeViewModel>> ObterPorId(Guid id)
        {
            var escolaridadeViewModel = await ObterTipoDocumento(id);

            if (escolaridadeViewModel == null) return NotFound();

            return CustomResponse(escolaridadeViewModel);
        }

        [ClaimsAuthorize("Admin", "1")]
        [HttpPost]
        public async Task<ActionResult<EscolaridadeViewModel>> Adicionar(EscolaridadeViewModel escolaridadeViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            if (escolaridadeViewModel.Id.ToString() == "00000000-0000-0000-0000-000000000000")
            {
                escolaridadeViewModel.Id = Guid.NewGuid();
            }

            await _escolaridadeService.Adicionar(_mapper.Map<Escolaridade>(escolaridadeViewModel));

            return CustomResponse(escolaridadeViewModel);
        }


        [ClaimsAuthorize("Admin", "1")]
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Atualizar(Guid id, EscolaridadeViewModel escolaridadeViewModel)
        {
            try
            {
                if (id != escolaridadeViewModel.Id)
                {
                    NotificarErro("Os ids informados não são iguais!");
                    return CustomResponse();
                }

                if (escolaridadeViewModel.Id.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    NotificarErro("ID não informado!");
                    return CustomResponse();
                }


                if (!ModelState.IsValid) return CustomResponse(ModelState);


                await _escolaridadeService.Atualizar(_mapper.Map<Escolaridade>(escolaridadeViewModel));

                return CustomResponse(escolaridadeViewModel);
            }
            catch (Exception ex)
            {
                NotificarErro(ex.Message.ToString());
                return CustomResponse();
            }
        }

        private async Task<EscolaridadeViewModel> ObterTipoDocumento(Guid id)
        {
            return _mapper.Map<EscolaridadeViewModel>(await _escolaridadeRepository.ObterPorId(id));
        }



    }
}