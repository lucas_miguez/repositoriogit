﻿using System;
using System.Linq;
using System.Threading.Tasks;
using De1vIO.Business.Models.Validations;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Business.Models.Validations;


namespace DevIO.Business.Services
{
    public class TipoDocumentoService : BaseService, ITipoDocumentoService
    {
        private readonly ITipoDocumentoRepository _tipoDocumentoRepository;
        private readonly IUser _user;

        public TipoDocumentoService(ITipoDocumentoRepository tipoDocumentoRepository,
                              INotificador notificador,
                              IUser user) : base(notificador)
        {
            _tipoDocumentoRepository = tipoDocumentoRepository;
            _user = user;
        }

        public async Task Adicionar(TipoDocumento tipoDocumento)
        {
            if (!ExecutarValidacao(new TipoDocumentoValidation(), tipoDocumento)) return;

            //var user = _user.GetUserId();

            await _tipoDocumentoRepository.Adicionar(tipoDocumento);
        }

        public async Task Atualizar(TipoDocumento tipoDocumento)
        {
            if (!ExecutarValidacao(new TipoDocumentoValidation(), tipoDocumento)) return ;

            if (_tipoDocumentoRepository.Buscar(f => f.Descricao == tipoDocumento.Descricao && f.Id != tipoDocumento.Id).Result.Any())
            {
                Notificar("Já existe um Tipo de Documento com esta descrição infomado.");
                return;
            }


            await _tipoDocumentoRepository.Atualizar(tipoDocumento);
            
        }

        public void Dispose()
        {
            _tipoDocumentoRepository?.Dispose();
        }
    }
}