﻿using System;
using System.Linq;
using System.Threading.Tasks;
using De1vIO.Business.Models.Validations;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Business.Models.Validations;

namespace DevIO.Business.Services
{
    public class EscolaridadeService : BaseService, IEscolaridadeService
    {
        private readonly IEscolaridadeRepository _escolaridadeRepository;
        private readonly IUser _user;

        public EscolaridadeService(IEscolaridadeRepository escolaridadeRepository,
                              INotificador notificador,
                              IUser user) : base(notificador)
        {
            _escolaridadeRepository = escolaridadeRepository;
            _user = user;
        }

        public async Task Adicionar(Escolaridade tipoDocumento)
        {
            if (!ExecutarValidacao(new EscolaridadeValidation(), tipoDocumento)) return;

            //var user = _user.GetUserId();

            await _escolaridadeRepository.Adicionar(tipoDocumento);
        }

        public async Task Atualizar(Escolaridade escolaridade)
        {
            if (!ExecutarValidacao(new EscolaridadeValidation(), escolaridade)) return;

            if (_escolaridadeRepository.Buscar(f => f.Descricao == escolaridade.Descricao && f.Id != escolaridade.Id).Result.Any())
            {
                Notificar("Já existe uma Escolaridade com esta descrição infomado.");
                return;
            }


            await _escolaridadeRepository.Atualizar(escolaridade);

        }

        public void Dispose()
        {
            _escolaridadeRepository?.Dispose();
        }
    }
}
