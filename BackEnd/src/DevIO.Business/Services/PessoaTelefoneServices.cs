﻿using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Services
{
    public class PessoaTelefoneServices : BaseService, IPessoaTelefoneServices
    {
        private readonly IPessoaTelefoneRepository _pessoaTelefoneRepository;
        
        public PessoaTelefoneServices(IPessoaTelefoneRepository pessoaTelefoneRepository,

                                 INotificador notificador) : base(notificador)
        {
            _pessoaTelefoneRepository = pessoaTelefoneRepository;

        }

        public Task<bool> Adicionar(PessoaTelefone pessoaTelefone)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Atualizar(PessoaTelefone pessoaTelefone)
        {
            throw new NotImplementedException();
        }

        public Task AtualizarPessoaTelefone(PessoaTelefone pessoaTelefone)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<bool> Remover(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
