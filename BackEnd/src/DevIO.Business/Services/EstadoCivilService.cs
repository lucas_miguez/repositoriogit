﻿using System;
using System.Linq;
using System.Threading.Tasks;
using De1vIO.Business.Models.Validations;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Business.Models.Validations;

namespace DevIO.Business.Services
{
    public class EstadoCivilService : BaseService, IEstadoCivilService
    {
        private readonly IEstadoCivilRepository _EstadoCivilRepository;
        private readonly IUser _user;

        public EstadoCivilService(IEstadoCivilRepository EstadoCivilRepository,
                              INotificador notificador,
                              IUser user) : base(notificador)
        {
            _EstadoCivilRepository = EstadoCivilRepository;
            _user = user;
        }

        public async Task Adicionar(EstadoCivil tipoDocumento)
        {
            if (!ExecutarValidacao(new EstadoCivilValidation(), tipoDocumento)) return;

            //var user = _user.GetUserId();

            await _EstadoCivilRepository.Adicionar(tipoDocumento);
        }

        public async Task Atualizar(EstadoCivil EstadoCivil)
        {
            if (!ExecutarValidacao(new EstadoCivilValidation(), EstadoCivil)) return;

            if (_EstadoCivilRepository.Buscar(f => f.Descricao == EstadoCivil.Descricao && f.Id != EstadoCivil.Id).Result.Any())
            {
                Notificar("Já existe uma EstadoCivil com esta descrição infomado.");
                return;
            }


            await _EstadoCivilRepository.Atualizar(EstadoCivil);

        }

        public void Dispose()
        {
            _EstadoCivilRepository?.Dispose();
        }
    }
}
