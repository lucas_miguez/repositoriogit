﻿using System;
using System.Linq;
using System.Threading.Tasks;
using De1vIO.Business.Models.Validations;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Business.Models.Validations;

namespace DevIO.Business.Services
{
    public class ProfissaoService : BaseService, IProfissaoService
    {
        private readonly IProfissaoRepository _ProfissaoRepository;
        private readonly IUser _user;

        public ProfissaoService(IProfissaoRepository ProfissaoRepository,
                              INotificador notificador,
                              IUser user) : base(notificador)
        {
            _ProfissaoRepository = ProfissaoRepository;
            _user = user;
        }

        public async Task Adicionar(Profissao Profissao)
        {
            if (!ExecutarValidacao(new ProfissaoValidation(), Profissao)) return;

            //var user = _user.GetUserId();

            await _ProfissaoRepository.Adicionar(Profissao);
        }

        public async Task Atualizar(Profissao Profissao)
        {
            if (!ExecutarValidacao(new ProfissaoValidation(), Profissao)) return;

            if (_ProfissaoRepository.Buscar(f => f.Descricao == Profissao.Descricao && f.Id != Profissao.Id).Result.Any())
            {
                Notificar("Já existe uma Profissao com esta descrição infomado.");
                return;
            }


            await _ProfissaoRepository.Atualizar(Profissao);

        }

        public void Dispose()
        {
            _ProfissaoRepository?.Dispose();
        }
    }
}
