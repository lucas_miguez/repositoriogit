﻿using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Services
{
    public class TipoEnderecoServices : BaseService, ItipoEnderecoServices
    {
        private readonly ITipoEnderecoRepository _tipoEnderecoRepository;
        private readonly IEnderecoRepository _enderecoRepository;

        public TipoEnderecoServices(ITipoEnderecoRepository tipoEnderecoRepository,
                               
                                 INotificador notificador) : base(notificador)
        {
            _tipoEnderecoRepository = tipoEnderecoRepository;
            
        }

        public Task Adicionar(TipoEndereco tipoEndereco)
        {
            throw new NotImplementedException();
        }

        public Task Atualizar(TipoEndereco tipoEndereco)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task Remover(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
