﻿using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Services
{
    public class EscolaridadeServices : BaseService, IEscolaridadeServices
    {
        private readonly IEscolaridadeRepository _escolaridadeRepository;
        public EscolaridadeServices(IEscolaridadeRepository escolaridadeRepository,

                               INotificador notificador) : base(notificador)
        {
            _escolaridadeRepository = escolaridadeRepository;

        }

        public Task<bool> Adicionar(Escolaridade escolaridade)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Atualizar(Escolaridade escolaridade)
        {
            throw new NotImplementedException();
        }

        public Task AtualizarEndereco(Escolaridade escolaridade)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<bool> Remover(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
