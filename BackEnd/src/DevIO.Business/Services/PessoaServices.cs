﻿using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Services
{
    public class PessoaServices : BaseService, IPessoaServices
    {
        private readonly IPessoaRepository _pessoaRepository;

        public PessoaServices(IPessoaRepository pessoaRepository,

                                 INotificador notificador) : base(notificador)
        {
            _pessoaRepository = pessoaRepository;

        }

        public Task Adicionar(Pessoa pessoa)
        {
            throw new NotImplementedException();
        }

        public Task Atualizar(Pessoa pessoa)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task Remover(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
