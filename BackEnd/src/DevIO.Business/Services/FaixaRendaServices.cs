﻿using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Services
{
    public class FaixaRendaServices : BaseService, IFaixaRendaServices
    {
        private readonly IFaixaRendaRepository _faixaRendaRepository;

        public FaixaRendaServices(IFaixaRendaRepository faixaRendaRepository,

                                 INotificador notificador) : base(notificador)
        {
            _faixaRendaRepository = faixaRendaRepository;

        }

        public Task<bool> Adicionar(FaixaRenda faixaRenda)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Atualizar(FaixaRenda faixaRenda)
        {
            throw new NotImplementedException();
        }

        public Task AtualizarEndereco(FaixaRenda faixaRenda)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<bool> Remover(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
