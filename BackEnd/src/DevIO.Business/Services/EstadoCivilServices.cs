﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DevIO.Business.Intefaces;
using DevIO.Business.Models;
using DevIO.Business.Models.Validations;

namespace DevIO.Business.Services
{
    public class EstadoCivilServices : BaseService, IEstadoCivilServices
    {
        private readonly IEstadoCivilRepository _estadoCivilRepository;
        public EstadoCivilServices(IEstadoCivilRepository estadoCivilRepository,
                              
                               INotificador notificador) : base(notificador)
        {
            _estadoCivilRepository = estadoCivilRepository;
           
        }
        public Task<bool> Adicionar(EstadoCivil estadoCivil)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Atualizar(EstadoCivil estadoCivil)
        {
            throw new NotImplementedException();
        }

        public Task AtualizarEstadoCivil(EstadoCivil estadoCivil)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<bool> Remover(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
