﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DevIO.Business.Models;

namespace DevIO.Business.Intefaces
{
    public interface ITipoDocumentoRepository : IRepository<TipoDocumento>
    {   
        Task<TipoDocumento> ObterTipoDocumento(Guid id);
    }
}
