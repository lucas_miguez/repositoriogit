﻿using System;
using System.Threading.Tasks;
using DevIO.Business.Models;
namespace DevIO.Business.Intefaces
{
    public interface IEstadoCivilService : IDisposable
    {
        Task Adicionar(EstadoCivil produto);
        Task Atualizar(EstadoCivil produto);
    }
}
