﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
    public interface IPessoaRepository : IRepository<Pessoa>
    {
        Task<IEnumerable<Pessoa>> ObterPessoa(Guid PessoaId);
        Task<IEnumerable<Pessoa>> ObterPessoas();
        Task<Produto> ObterPessoas(Guid id);
    }
    
}
