﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
    public interface IPessoaTelefoneRepository: IRepository<PessoaTelefone>
    {
        Task<IEnumerable<PessoaTelefone>> ObterPessoaTelefone(Guid PessoaTelefonerId);
        Task<IEnumerable<PessoaTelefone>> ObterPessoaTelefone();
        Task<PessoaTelefone> ObterPessoaTelefonepessoa(Guid id);
    }
}
