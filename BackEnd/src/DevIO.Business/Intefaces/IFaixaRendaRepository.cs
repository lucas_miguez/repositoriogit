﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
    public interface IFaixaRendaRepository: IRepository<FaixaRenda>
    {

        Task<FaixaRenda> ObterFornecedorEndereco(Guid id);
        Task<FaixaRenda> ObterFornecedorProdutosEndereco(Guid id);
    }
}
