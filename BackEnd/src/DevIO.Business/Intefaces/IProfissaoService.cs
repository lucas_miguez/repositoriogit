﻿using System;
using System.Threading.Tasks;
using DevIO.Business.Models;

namespace DevIO.Business.Intefaces
{
    public interface IProfissaoService : IDisposable
    {
        Task Adicionar(Profissao produto);
        Task Atualizar(Profissao produto);
    }
}
