﻿using DevIO.Business.Models;
using System;
using System;
using System.Threading.Tasks;
using DevIO.Business.Models;

namespace DevIO.Business.Intefaces
{
    public interface IEscolaridadeServices : IDisposable
    {
        Task<bool> Adicionar(Escolaridade escolaridade);
        Task<bool> Atualizar(Escolaridade escolaridade);
        Task<bool> Remover(Guid id);

        Task AtualizarEndereco(Escolaridade escolaridade);
    }
   

}
