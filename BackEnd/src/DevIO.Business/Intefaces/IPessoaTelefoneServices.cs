﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
    public interface IPessoaTelefoneServices : IDisposable
    {
        Task<bool> Adicionar(PessoaTelefone pessoaTelefone);
        Task<bool> Atualizar(PessoaTelefone pessoaTelefone);
        Task<bool> Remover(Guid id);

        Task AtualizarPessoaTelefone(PessoaTelefone pessoaTelefone);
    }
    
}
