﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
    public interface IFaixaRendaServices:IDisposable
    {

        Task<bool> Adicionar(FaixaRenda faixaRenda);
        Task<bool> Atualizar(FaixaRenda faixaRenda);
        Task<bool> Remover(Guid id);

        Task AtualizarEndereco(FaixaRenda faixaRenda);
    }
}
