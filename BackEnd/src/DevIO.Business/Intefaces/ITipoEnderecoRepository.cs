﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
    public interface ITipoEnderecoRepository : IRepository<TipoEndereco>
    {
        Task<IEnumerable<TipoEndereco>> ObterTipoEndereco(Guid TipoEnderecoId);
        Task<IEnumerable<TipoEndereco>> ObterTipoEndereco();
        Task<TipoEndereco> ObterTipoEnderecoEnd(Guid id);
    
    }
}
