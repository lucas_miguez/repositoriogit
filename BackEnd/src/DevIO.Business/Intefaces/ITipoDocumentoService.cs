﻿using System;
using System.Threading.Tasks;
using DevIO.Business.Models;

namespace DevIO.Business.Intefaces
{
    public interface ITipoDocumentoService : IDisposable
    {
        Task Adicionar(TipoDocumento produto);
        Task Atualizar(TipoDocumento produto);
    }
}