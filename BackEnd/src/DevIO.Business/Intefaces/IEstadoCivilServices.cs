﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
   
   public interface IEstadoCivilServices : IDisposable
    {
        Task<bool> Adicionar(EstadoCivil estadoCivil);
        Task<bool> Atualizar(EstadoCivil estadoCivil);
        Task<bool> Remover(Guid id);

        Task AtualizarEstadoCivil(EstadoCivil estadoCivil);
    }
}
