﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
    public interface IPessoaServices : IDisposable
    {
        Task Adicionar(Pessoa pessoa);
        Task Atualizar(Pessoa pessoa);
        Task Remover(Guid id);
    }
   
}
