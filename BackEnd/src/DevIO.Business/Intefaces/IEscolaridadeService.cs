﻿using System;
using System.Threading.Tasks;
using DevIO.Business.Models;

namespace DevIO.Business.Intefaces
{
    public interface IEscolaridadeService : IDisposable
    {
        Task Adicionar(Escolaridade produto);
        Task Atualizar(Escolaridade produto);
    }
}
