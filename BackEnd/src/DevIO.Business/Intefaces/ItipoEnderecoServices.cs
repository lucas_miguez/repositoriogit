﻿using DevIO.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevIO.Business.Intefaces
{
    public interface ItipoEnderecoServices : IDisposable
    {
        Task Adicionar(TipoEndereco tipoEndereco);
        Task Atualizar(TipoEndereco tipoEndereco);
        Task Remover(Guid id);
    }
   
}
