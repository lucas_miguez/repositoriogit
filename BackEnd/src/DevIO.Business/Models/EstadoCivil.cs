﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevIO.Business.Models
{
    public class EstadoCivil : Entity
    {
        public string Descricao { get; set; }

        public Boolean Ativo { get; set; }
    }
}
