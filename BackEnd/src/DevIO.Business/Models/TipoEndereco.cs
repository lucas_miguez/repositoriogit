﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevIO.Business.Models
{
	public class TipoEndereco : Entity
	{
		public int TipoEndereco_Codigo { get; set; }
		public string TipoEndereco_Descricao  { get; set; }


	    public Boolean TipoEndereco_Ativo  { get; set; }

		public int TipoEndereco_UsuCodCriacao  { get; set; }
 
		 public DateTime TipoEndereco_DataCriacao  { get; set; }
 
		  public int TipoEndereco_UsuCodAlteracao  { get; set; }
		 public DateTime TipoEndereco_DataAlteracao  { get; set; }
    }
}
