﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevIO.Business.Models
{
    public class Pessoa : Entity
    {

        public string  Nome { get; set; }

        public string NomeFantasia { get; set; }
        public string TipoPessoa { get; set; }
        public string CpfCnpj { get; set; }
        public string RG { get; set; }
        public DateTime DataNascFund { get; set; }

        public string Sexo { get; set; }
        
        //public [EstadoCivil_Codigo] [uniqueidentifier] NULL,

        //public [Escolaridade_Codigo] [uniqueidentifier] NULL,

        //public [Profissao_Codigo] [uniqueidentifier] NULL,

        //public [Email] [varchar] (150) NOT NULL,


        // public [Ativo] 
        

    }
}
