﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace DevIO.Business.Models
{
    public class TipoDocumento : Entity
    {
        public string Descricao { get; set; }

        public Boolean Ativo { get; set; }

    }
}