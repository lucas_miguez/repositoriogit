﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevIO.Business.Models
{
    public class FaixaRenda : Entity 
    {
        
        public int FaixaRenda_Codigo { get; set; }

        public string FaixaRenda_Descricao { get; set; }

        public Boolean FaixaRenda_Ativo { get; set; }


        public int FaixaRenda_UsuCodCriacao { get; set; }

        public DateTime FaixaRenda_DataCriacao { get; set; }

        public int FaixaRenda_UsuCodAlteracao { get; set; }
    public DateTime FaixaRenda_DataAlteracao  { get; set; }
    }
}
