﻿using DevIO.Business.Models;
using FluentValidation;

namespace DevIO.Business.Models.Validations
{
 
    public class EscolaridadeValidation : AbstractValidator<Escolaridade>
    {
        public EscolaridadeValidation()
        {
            RuleFor(c => c.Descricao)
                .NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido")
                .Length(2, 200).WithMessage("O campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");
        }
    }
}
