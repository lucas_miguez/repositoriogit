﻿using DevIO.Business.Models;
using FluentValidation;

namespace De1vIO.Business.Models.Validations
{
    public class TipoDocumentoValidation : AbstractValidator<TipoDocumento>
    {
        public TipoDocumentoValidation()
        {
            RuleFor(c => c.Descricao)
                .NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido")
                .Length(2, 200).WithMessage("O campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");
        }
    }
}
